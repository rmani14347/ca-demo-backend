package com.abc.util;

/**
 * This is a enum which has all the status codes
 * @author 
 *
 */
public enum StatusCodes {
   OK(1),ERROR(-1),UN_AUTH(2),NOT_FOUND(3);
	int value;
	private StatusCodes(int value) {
		this.value = value;
	}
	public int getValue() {
		return value;
	}

}
