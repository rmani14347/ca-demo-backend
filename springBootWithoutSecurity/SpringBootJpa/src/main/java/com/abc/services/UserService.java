package com.abc.services;


import com.abc.entities.User;


public interface UserService {

	public User getUserByUsername(String username);

	public User save(User user);
	public User update(User user);
	public User authentication(String username , String password);

}
