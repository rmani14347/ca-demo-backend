package com.abc.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.entities.User;
import com.abc.payload.ResponseObject;
import com.abc.services.UserService;
import com.abc.util.StatusCodes;

@CrossOrigin(origins = "*", maxAge = 3600L)
@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private UserService userService;


	/**
	 * this method is used for registering the user
	 * 
	 * @param user
	 * @return
	 */
	@PostMapping("/registerUser")
	public ResponseEntity<ResponseObject> registerUser(@RequestBody User user) {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		// verifying that email is already existed or not
		User userByUsername = userService.getUserByUsername(user.getUsername());
		User savedUser;

		if (userByUsername == null) {
			savedUser = userService.save(user);
			if (savedUser != null) {

				responseObject.setResponse(savedUser);
				responseObject.setStatusCode(StatusCodes.OK.getValue());
			} else {
				responseObject.setResponse("Something wrong");
				responseObject.setStatusCode(StatusCodes.ERROR.getValue());
			}
		} else {
			responseObject.setResponse("user with this User Name already exists");
			
			responseObject.setStatusCode(StatusCodes.NOT_FOUND.getValue());
		}
		// adding response object and status code of response entity class

		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;

	}

	/**
	 * this method is used to get the details of the particular user by using the id
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/getUserByUsername/{username}")
	public ResponseEntity<ResponseObject> getUserByUsername(@PathVariable("username") String username) {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		User userByUsername = userService.getUserByUsername(username);

		if (userByUsername != null) {

			responseObject.setResponse(userByUsername);
			responseObject.setStatusCode(StatusCodes.OK.getValue());
		} else {

			responseObject.setResponse("User with this User name does not exists");
			responseObject.setStatusCode(StatusCodes.NOT_FOUND.getValue());

		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
	}
	/**
	  * this method is used for updating the profile of the user 
	  * @param user
	  * @return
	  */
	@PutMapping("/updateUser")
	@CrossOrigin
	public ResponseEntity<ResponseObject> updateUser(@RequestBody User user){
		
		ResponseEntity<ResponseObject> responseEntity = null;
		User userByName = userService.getUserByUsername(user.getUsername());
		ResponseObject responseObject = new ResponseObject();
		User updatedUser = userService.update(user);
		if(userByName != null) {
			updatedUser = userService.update(user);
			if(updatedUser!=null) {
			responseObject.setResponse(updatedUser);
			responseObject.setStatusCode(1);
			}else {
				responseObject.setResponse("Something wrong");
				responseObject.setStatusCode(-1);
			}
		}else {
			responseObject.setResponse("user with this Email does not exists");
			responseObject.setStatusCode(-2);
		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
		
	}
	@PostMapping("/loginUser")
	public ResponseEntity<ResponseObject> loginUser(@RequestBody User user) {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		User userByUsername = userService.authentication(user.getUsername(),user.getPassword());

		if (userByUsername != null) {

			responseObject.setResponse(userByUsername);
			responseObject.setStatusCode(StatusCodes.OK.getValue());
		} else {

			responseObject.setResponse("Invalid Credentials");
			responseObject.setStatusCode(StatusCodes.NOT_FOUND.getValue());

		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
	}

	}
