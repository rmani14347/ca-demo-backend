package com.abc.entities;
//package com.abc.bean;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.ColumnDefault;
//
//@Entity
//@Table(name = "user_details_tbl")
//public class UserDetails {
//	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private int id;
//	@Column
//	private String firstName;
//	@Column
//	private String lastName;
//	@Column
//	private String username;
//	@Column
//	private String password;
//	@Column
//	private String organization;
//	@Column
//	private String email;
//	@Column
//	private String phone;
//	@Column
//	private String role;
//	
//	
//	/**
//	 * @return the role
//	 */
//	public String getRole() {
//		return role;
//	}
//	/**
//	 * @param role the role to set
//	 */
//	public void setRole(String role) {
//		this.role = role;
//	}
//	/**
//	 * @return the id
//	 */
//	public int getId() {
//		return id;
//	}
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(int id) {
//		this.id = id;
//	}
//	/**
//	 * @return the firstName
//	 */
//	public String getFirstName() {
//		return firstName;
//	}
//	/**
//	 * @param firstName the firstName to set
//	 */
//	public void setFirstName(String firstName) {
//		this.firstName = firstName;
//	}
//	/**
//	 * @return the lastName
//	 */
//	public String getLastName() {
//		return lastName;
//	}
//	/**
//	 * @param lastName the lastName to set
//	 */
//	public void setLastName(String lastName) {
//		this.lastName = lastName;
//	}
//	/**
//	 * @return the username
//	 */
//	public String getUsername() {
//		return username;
//	}
//	/**
//	 * @param username the username to set
//	 */
//	public void setUsername(String username) {
//		this.username = username;
//	}
//	/**
//	 * @return the password
//	 */
//	public String getPassword() {
//		return password;
//	}
//	/**
//	 * @param password the password to set
//	 */
//	public void setPassword(String password) {
//		this.password = password;
//	}
//	/**
//	 * @return the organization
//	 */
//	public String getOrganization() {
//		return organization;
//	}
//	/**
//	 * @param organization the organization to set
//	 */
//	public void setOrganization(String organization) {
//		this.organization = organization;
//	}
//	/**
//	 * @return the email
//	 */
//	public String getEmail() {
//		return email;
//	}
//	/**
//	 * @param email the email to set
//	 */
//	public void setEmail(String email) {
//		this.email = email;
//	}
//	/**
//	 * @return the phone
//	 */
//	public String getPhone() {
//		return phone;
//	}
//	/**
//	 * @param phone the phone to set
//	 */
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//	
//
//	
//
//}
