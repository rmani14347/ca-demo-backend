package com.abc.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.abc.entities.Address;

@Repository
public interface AdressRepository extends CrudRepository<Address, Integer>  {
  
}
