package com.abc.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.abc.entities.User;

/**
 * This interface is used as a user repository
 * @author 
 *
 */
@Repository
public interface UserDao extends JpaRepository<User, Integer>{
	
	public User findByUsername(String username);
	
	@Modifying
	 @Query("UPDATE User u SET u.departmentId = :departmentId WHERE u.id = :userId")
	 int updateDepartment(@Param("departmentId")int departmentId,@Param("userId") int userId);

//	 int updateDepartments(int departmentId,int userId);
	
     Page<User> findAll(Pageable pageable);

}
