<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   
 <%@ taglib prefix="ui" uri="customeTagLib" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Users</title>
<style>
	table,th,td {
		border : 1px solid black;
		border-collapse: collapse;
	}
</style>
</head>
<body>
<h2>All Records</h2>
<c:if test="${userDetailsList.size() == 0 }">
<h2>No users in database</h2>
</c:if>
<ul>
  		<table>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Organization</th>
							<th>Email</th>
							<th>Phone</th>
						</tr>
 <c:forEach items="${userDetailsList}" var="user">

						<tr>
							<td>${user.firstName } </td>
							<td>${user.lastName }</td>
							<td>${user.organization }</td>
							<td>${user.email }</td>
							<td>${user.phone }</td>
						</tr>
				
 </c:forEach>
 	</table>
</ul>
<div >
<c:if test="${page eq 'allUsers'}">
<h2>Sort by First Name</h2>
<ui:sort userDetailsList="${userDetailsList}"/>
</c:if>
</div>
</body>
</html>