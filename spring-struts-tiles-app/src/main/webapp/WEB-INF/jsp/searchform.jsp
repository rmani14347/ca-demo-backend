<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="ui" uri="customeTagLib"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
	.styled table tr td {
		border : 1px solid black;
		border-collapse: collapse;
	}
</style>
</head>
<body>
	<div>
		<form action="search.html" method="post">
			<table>
				<tr>
					<td>UserName</td>
					<td><input type="text" name="username" /></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2"><input type="submit" value="search" /></td>
				</tr>
			</table>
		</form>
	</div>
	<div>
		<c:choose>
			<c:when test="${userDetails == null}">
				<h2>No Records Found</h2>
			</c:when>
			<c:otherwise>
			<!-- 	<div class = "styled">
					<table>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Organization</th>
							<th>Email</th>
							<th>Phone</th>
						</tr>
						<tr>
							<td>{userDetails.firstName } </td>
							<td>${userDetails.lastName }</td>
							<td>${userDetails.organization }</td>
							<td>${userDetails.email }</td>
							<td>${userDetails.phone }</td>

						</tr>
					</table>
				</div>  -->
				<h1>User Details:</h1>
				<p><b>First Name:</b>&nbsp;${userDetails.firstName}</p>
				<p><b>Last Name:</b>&nbsp;${userDetails.lastName}</p>
				<p><b>Organization</b>&nbsp;${userDetails.organization}</p>
				<p><b>Email:</b>&nbsp;${userDetails.email}</p>
				<p><b>Phone:</b>&nbsp;${userDetails.phone}</p>
				<div class = "styled">
					<h2>From Custom Tag</h2>
					<ui:displayUser userDetails="${userDetails }" />
				</div>

			</c:otherwise>
		</c:choose>
	</div>



</body>
</html>