<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>     	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
 <form:form commandName="register" method="post">
   <table align="center" >
		<tr>
			<td>First Name :</td>
			<td><form:input path="firstName" required="required"/></td>
		</tr>
		<tr>
			<td>Last Name :</td>
			<td><form:input path="lastName" required="required" /></td>
		</tr>
		<tr>
			<td>UserName</td>
			<td><form:input path="username" required="required"/></td>
		</tr>	
		<tr>
			<td>Password</td>
			<td><form:password path="password" required="required"/></td>
		</tr>	
		<tr>
			<td>Organization</td>
			<td><form:input path="organization" required="required" /></td>
		</tr>	
		<tr>
			<td>Email</td>
			<td><form:input path="email" required="required"/></td>
		</tr>	
		<tr>
			<td>Phone</td>
			<td><form:input path="phone" required="required" /></td>
		</tr>		
		<tr>
			<td></td>
			<td><input type="submit" value="register" /></td>
		</tr>
	</table>
  </form:form> 
  
  
</body>
  
</html>