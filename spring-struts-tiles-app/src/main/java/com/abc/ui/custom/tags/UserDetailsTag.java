package com.abc.ui.custom.tags;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.abc.bean.UserDetails;

public class UserDetailsTag extends SimpleTagSupport {
	
	private UserDetails userDetails;

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
	
	 @Override
	    public void doTag() throws JspException, IOException {
		 
	        JspWriter out =  getJspContext().getOut();
	        out.println("<html><body>");
	        out.println("<table><tr><th>First Name</th><th>Last Name</th><th>Email</th></tr>");
	        out.println("<tr>");
	        out.println("<td>"+userDetails.getFirstName()+"</td>");
	        out.println("<td>"+userDetails.getLastName()+"</td>");
	        out.println("<td>"+userDetails.getEmail()+"</td>");
	        out.println("</tr></table>");
	        out.println("</body></html>");
	    }

}
