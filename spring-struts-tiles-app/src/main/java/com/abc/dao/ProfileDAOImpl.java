package com.abc.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.abc.bean.UserDetails;
import com.abc.exceptions.MyDaoException;
import com.ibatis.sqlmap.client.SqlMapClient;

public class ProfileDAOImpl implements ProfileDAO {

	private SqlMapClient sqlMapClient;

	public void setSqlMapClient(SqlMapClient sqlMapClient) {
		this.sqlMapClient = sqlMapClient;
	}

	@Override
	public void createProfile(UserDetails userDetails) throws DataAccessException {
		try {
			 this.sqlMapClient.insert("addUser", userDetails);
		}
		catch (SQLException ex) {
			throw new MyDaoException(ex);
		}
		
	}

	@Override
	public UserDetails getUserByUserName(String username) {

		UserDetails userDetails =null;

		try {
			userDetails = (UserDetails) this.sqlMapClient.queryForObject("getUserByUserName", username);
		}
		catch (SQLException ex) {
			throw new MyDaoException(ex);
		}	
		return userDetails;
	}

	@Override
	public List<UserDetails> getAllUsers() {
		List<UserDetails> usersList = new ArrayList<UserDetails>();
		try {
			usersList = (List<UserDetails>) this.sqlMapClient.queryForList("getAllUsers");
		}
		catch (SQLException ex) {
			throw new MyDaoException(ex);
		}	
		return usersList;
	}

	@Override
	public List<UserDetails> sortByRegistrationDate() {
		List<UserDetails> usersList = new ArrayList<UserDetails>();
		try {
			usersList = (List<UserDetails>) this.sqlMapClient.queryForList("getAllUsersByDate");
		}
		catch (SQLException ex) {
			throw new MyDaoException(ex);
		}	
		return usersList;
	}

	@Override
	public boolean setDepartmentId(int userId,int departmentId) {

		UserDetails userDetails = new UserDetails();
		userDetails.setId(userId);
		userDetails.setDepartmnetID(departmentId);
		int status=0;
		try {
			status = sqlMapClient.update("setdepartmentid", userDetails);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(status == 0)
			return false;
		else
			return true;
	}

	@Override
	public UserDetails findById(int userId) {

		UserDetails userDetails =null;

		try {
			userDetails = (UserDetails) this.sqlMapClient.queryForObject("getUserByUserId", userId);
		}
		catch (SQLException ex) {
			throw new MyDaoException(ex);
		}	
		return userDetails;
	}
	@Override
	public int  updateUser(UserDetails userDetails) {
		int isUpdated;
		try {
			isUpdated  =  (int) this.sqlMapClient.update("updateUser", userDetails);
		}
		catch (SQLException ex) {
			throw new MyDaoException(ex);
		}
		return isUpdated;

	}

}
