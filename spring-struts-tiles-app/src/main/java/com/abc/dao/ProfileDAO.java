package com.abc.dao;

import java.util.List;

import com.abc.bean.UserDetails;

public interface ProfileDAO {
	
	void createProfile(UserDetails userDetails);
	UserDetails getUserByUserName(String username);
	List<UserDetails> getAllUsers();
	public List<UserDetails> sortByRegistrationDate();
	public boolean setDepartmentId(int userId,int departmentId);
	UserDetails findById(int userId);
	int updateUser(UserDetails userDetails);
}
