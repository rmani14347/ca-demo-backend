package com.abc.service;

import java.sql.Timestamp;
import java.util.List;

import com.abc.bean.UserDetails;
import com.abc.dao.ProfileDAO;


public class ProfileServiceImpl implements ProfileService {
	
	private ProfileDAO profileDAO;
	
	public void setProfileDAO(ProfileDAO profileDAO) {
		this.profileDAO = profileDAO;
	}
	
	@Override
	public int saveProfile(UserDetails userDetails) {
	
		UserDetails validateUser=searchByUsername(userDetails.getUsername());
		if(validateUser!=null) {
			return -1;
		}
		userDetails.setRegistrationDate(new Timestamp(System.currentTimeMillis()));
		 profileDAO.createProfile(userDetails);
		 return 1;
	}

	@Override
	public UserDetails searchByUsername(String username) {
	
		return profileDAO.getUserByUserName(username);
	}

	@Override
	public List<UserDetails> fetchAllUsers() {
		
		return profileDAO.getAllUsers();
	}
	
	@Override
	public List<UserDetails> sortByRegistrationDate() {
		return profileDAO.sortByRegistrationDate();
	}

	@Override
	public boolean setDepartmentId(int userId,int departmentId) {
		
		return profileDAO.setDepartmentId(userId, departmentId);
	}

	@Override
	public boolean updateDetails(UserDetails userDetails) {
		UserDetails profile = profileDAO.findById(userDetails.getId());
		boolean status = false;
		if(userDetails.getEmail() != profile.getEmail())
		{
			status = true;
			profile.setEmail(userDetails.getEmail());
		}
		if(userDetails.getFirstName() != profile.getFirstName())
		{
			status = true;
			profile.setFirstName(userDetails.getFirstName());
		}
		if(userDetails.getLastName() != profile.getLastName())
		{
			status = true;
			profile.setLastName(userDetails.getLastName());
		}
		if(userDetails.getOrganization() != profile.getOrganization())
		{
			status = true;
			profile.setOrganization(userDetails.getOrganization());
		}
		if(userDetails.getPhone() != profile.getPhone())
		{
			status = true;
			profile.setPhone(userDetails.getPhone());
		}
		if(userDetails.getUsername() != profile.getUsername())
		{
			status = true;
			profile.setUsername(userDetails.getUsername());
		}
		profileDAO.updateUser(profile);
		return status;
	}

}
