package com.abc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.abc.bean.UserDetails;
import com.abc.service.ProfileService;

public class EditSearchController extends AbstractController {

	private ProfileService profileService;
	
	public ProfileService getProfileService() {
		return profileService;
	}

	public void setProfileService(ProfileService profileService) {
		this.profileService = profileService;
	}

	
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		String username = request.getParameter("username");
		UserDetails result = profileService.searchByUsername(username);
		System.out.println("varun");
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("userDetails", result);
		modelAndView.addObject("attempt", 1);
		
		if(result == null) {
			modelAndView.setViewName("edit");
		} else {
			modelAndView.setViewName("updatedetails");
		}
		
		return modelAndView;
	}


	

}
