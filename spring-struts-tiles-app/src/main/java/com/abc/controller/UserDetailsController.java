package com.abc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.abc.bean.UserDetails;
import com.abc.service.ProfileService;

public class UserDetailsController extends AbstractController {
	
	private ProfileService profileService;

	public void setProfileService(ProfileService profileService) {
		this.profileService = profileService;
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		String username = req.getParameter("username");
		UserDetails userDetails = profileService.searchByUsername(username);
		ModelAndView mav = new ModelAndView("searchform");
		mav.addObject("userDetails", userDetails);
		return mav;
	}
	
//	@RequestMapping(value="/getAll", method=RequestMethod.GET)
//	public String getAllUsers(ModelMap map) {
//		List<UserDetails> userDetailsList = profileService.fetchAllUsers();
//		map.addAttribute("userDetailsList", userDetailsList);
//		return "allusers";		
//	}
}
