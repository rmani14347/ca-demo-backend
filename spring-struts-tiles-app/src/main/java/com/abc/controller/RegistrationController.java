package com.abc.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.abc.bean.UserDetails;
import com.abc.service.ProfileService;

@SuppressWarnings("deprecation")
public class RegistrationController extends SimpleFormController {
	
	private ProfileService profileService;
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		
		UserDetails userDetails = (UserDetails) command;
		int result=profileService.saveProfile(userDetails);
		ModelAndView mav = new ModelAndView(getSuccessView());
		mav.addObject("userID", userDetails.getUsername());
		mav.addObject("status",result);
		return mav;
	}

	@Override
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		UserDetails userDetails = (UserDetails) command;
		Map refData = new HashMap(); // refdata holds information that are not specific to the bean, like user logged in
		System.out.println("Errors:"+errors.getErrorCount());
		return refData;
	}

	public void setProfileService(ProfileService profileService) {
		this.profileService = profileService;
	}

	

}
