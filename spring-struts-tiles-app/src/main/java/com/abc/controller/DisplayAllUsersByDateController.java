package com.abc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.abc.bean.UserDetails;
import com.abc.service.ProfileService;

public class DisplayAllUsersByDateController extends AbstractController{

		
		private ProfileService profileService;

		public void setProfileService(ProfileService profileService) {
			this.profileService = profileService;
		}

		@Override
		protected ModelAndView handleRequestInternal(HttpServletRequest req, HttpServletResponse resp) throws Exception {
			List<UserDetails> userDetailsList = profileService.sortByRegistrationDate();
			System.out.println(userDetailsList.get(0).getRegistrationDate());
			ModelAndView mav = new ModelAndView("allusers");
			mav.addObject("userDetailsList", userDetailsList);
			mav.addObject("page", "allUsersSortByDate");
			return mav;
		}

	
}
