package com.abc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.abc.service.ProfileService;

public class DepartmentController extends AbstractController {

	private ProfileService profileService;

	public void setProfileService(ProfileService profileService) {
		this.profileService = profileService;
	}
	
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		int attempts = 1;
		int userId=0;
		int departmentId=0;
		boolean status = false;
		
		if(request.getParameter("userid")!=null || request.getParameter("departmnetid")!=null)
		{
			userId = Integer.parseInt(request.getParameter("userid"));
			departmentId = Integer.parseInt(request.getParameter("departmnetid"));
			status = profileService.setDepartmentId(userId, departmentId);
		}
		
		
		ModelAndView modelAndView = new ModelAndView("setdepartmnet");
		System.out.println(userId+" "+departmentId);
		modelAndView.addObject("status",status);
		modelAndView.addObject("attempts",attempts);
		return modelAndView;
	}

}
