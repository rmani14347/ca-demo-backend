package com.abc.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.abc.bean.UserDetails;
import com.abc.service.ProfileService;

public class UpdateDetailsController extends SimpleFormController{

	private ProfileService profileService;
	
	
	public ProfileService getProfileService() {
		return profileService;
	}

	public void setProfileService(ProfileService profileService) {
		this.profileService = profileService;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response,Object command,BindException errors) throws Exception {
		UserDetails userDetails = (UserDetails) command;
		
		boolean result = profileService.updateDetails(userDetails);
		
		ModelAndView modelAndView = new ModelAndView(getSuccessView());
		modelAndView.addObject("result", result);
		modelAndView.addObject("attempt", 1);
		return modelAndView;
	}
	
	@Override
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		UserDetails userDetails = (UserDetails) command;
		Map refData = new HashMap(); // refdata holds information that are not specific to the bean, like user logged in
		System.out.println("Errors:"+errors.getErrorCount());
		return refData;
	}


	
}
