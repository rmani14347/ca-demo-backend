package com.abc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.abc.bean.UserDetails;
import com.abc.service.ProfileService;

public class DisplayAllUsersController extends AbstractController {
	
	private ProfileService profileService;

	public void setProfileService(ProfileService profileService) {
		this.profileService = profileService;
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		List<UserDetails> userDetailsList = profileService.fetchAllUsers();
		ModelAndView mav = new ModelAndView("allusers");
		mav.addObject("userDetailsList", userDetailsList);
		mav.addObject("page", "allUsers");
		return mav;
	}
	
}
