package com.abc.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.abc.entities.User;

/**
 * This interface is used as a user repository
 * @author 
 *
 */
@Repository
public interface UserDao extends JpaRepository<User, Integer>{
	
	public User findByUsername(String username);
	
     Page<User> findAll(Pageable pageable);

}
