package com.abc.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.abc.entities.Policy;

/**
 * This interface is used as a policy repository
 * @author 
 *
 */
@Repository
public interface PolicyDao extends CrudRepository<Policy, Integer>{

	
//	@Query("SELECT  u.firstName,u.lastName,u.email,u.phone,p.policyName,p.policyDescription FROM Policy p INNER JOIN User_tbl u ON p.policyId=u.policyId")
//	public List<Object> getAllUsersPolicy();
	
}