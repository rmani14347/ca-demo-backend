package com.abc.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.abc.entities.User;

/**
 * This Class Implements named Query from JPA
 * @author IMVIZAG
 *
 */
@Repository
@Transactional
public class UserDaoImpl implements UserDaoNamedQuery{

	@PersistenceContext
    private EntityManager manager;
	
	/**
	 * Updating user Department
	 * @param user id
	 * @param department id
	 * @return int value
	 */
	@Override
	public int updateDepartment(Integer userId, Integer departmentId) {
		try
        {
            manager.createNamedQuery("updateUserDepartment", User.class)
            .setParameter(1, userId)
            .setParameter(2, departmentId)
            .executeUpdate();
             
            return 1;
        }
        catch (Exception e)
        {
            return 0;
        }
	}

}
