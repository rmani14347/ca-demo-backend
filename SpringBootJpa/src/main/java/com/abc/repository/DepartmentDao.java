package com.abc.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.abc.entities.Department;

/**
 * This interface is used as a department repository
 * @author 
 *
 */
@Repository
public interface DepartmentDao extends CrudRepository<Department, Integer> {
	
}
