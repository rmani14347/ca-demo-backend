package com.abc.repository;

public interface UserDaoNamedQuery {
	int updateDepartment(Integer userId, Integer departmentId);
}
