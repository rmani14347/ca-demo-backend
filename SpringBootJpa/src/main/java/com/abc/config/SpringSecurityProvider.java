package com.abc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.abc.security.MyAccessDeniedHandler;
import com.abc.security.MyAuthFailureHandler;
import com.abc.security.MyFaillureHandler;
import com.abc.security.MySuccessHandler;
import com.abc.services.SecurityService;

//@EnableWebSecurity
@Configuration
public class SpringSecurityProvider extends WebSecurityConfigurerAdapter {
	


	@Bean
	public SecurityService securityService() {
		return new SecurityService();
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(securityService());
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}
	
	@Bean
	public MySuccessHandler getSuccessHandler() {
		return new MySuccessHandler();
	}
	@Bean 
	public MyFaillureHandler getFaillureHandler() {
		return new MyFaillureHandler();
	}
	@Bean
	public MyAuthFailureHandler getAuthFaillureHandler() {
		return new MyAuthFailureHandler();
	}
	
	@Bean MyAccessDeniedHandler getAccessDeniedHandler() {
		return new MyAccessDeniedHandler();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(authenticationProvider());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues());
		http.authorizeRequests()
		.antMatchers("/api/user/registerUser").permitAll()
		.antMatchers("/login").permitAll()
		.antMatchers("/api/user/**").access("hasRole('ROLE_USER')")
		.antMatchers("/api/admin/**").access("hasRole('ROLE_ADMIN')")
		.anyRequest().permitAll()
		.and()
		.formLogin()
		.successHandler(getSuccessHandler())
		.failureHandler(getFaillureHandler())	
		.and()
		.exceptionHandling().accessDeniedHandler(getAccessDeniedHandler())
		.authenticationEntryPoint(getAuthFaillureHandler())
		.and()
		.httpBasic();
	}
	
	@Bean
	public CorsFilter corsFilter() {
	    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    final CorsConfiguration config = new CorsConfiguration();
	    config.setAllowCredentials(true);
	    config.addAllowedOrigin("*");
	    config.addAllowedHeader("*");
	    config.addAllowedMethod("OPTIONS");
	    config.addAllowedMethod("HEAD");
	    config.addAllowedMethod("GET");
	    config.addAllowedMethod("PUT");
	    config.addAllowedMethod("POST");
	    config.addAllowedMethod("DELETE");
	    config.addAllowedMethod("PATCH");
	    source.registerCorsConfiguration("/**", config);
	    return new CorsFilter(source);
	}
	
}
