package com.abc.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.abc.entities.Department;
import com.abc.entities.User;
import com.abc.repository.DepartmentDao;
import com.abc.repository.PolicyDao;
import com.abc.repository.UserDao;
import com.abc.repository.UserDaoNamedQuery;


/**
 * This class refers to admin service which calls the methods from AdminDAO
 * 
 * @author IMVIZAG
 *
 */
@Service
@Transactional
public class AdminServiceImpl implements AdminService{

	@Autowired
	private UserDao userDao;

	@Autowired
	private PolicyDao policyDao;
		  
	@Autowired
	private DepartmentDao deptDao;
	
	@Autowired
	private UserDaoNamedQuery deptUpdate;
	/**
	 * this method is referring to the display all user which was called by
	 * controller class
	 * 
	 * @return
	 */
	@Override
	public List<User> displayAllUsers(int page) {
		List<User> userList = new ArrayList<>();
		
		Pageable pageable = PageRequest.of(page, 5,Sort.Direction.ASC,"firstName");
		userDao.findAll(pageable).forEach(userList::add);
		
 		 
		return userList;
	}
	
	public long countOfUsersPerPage() {
		
		List<User> totalUserList = new ArrayList<>();
		userDao.findAll().forEach(totalUserList::add);
		
		int noOfUsers = (totalUserList.size())-1;
		double UsersPerPage = noOfUsers/5.0;
		long countPerPage = Math.round(UsersPerPage);
		
		return countPerPage;
	}


	/**
	 *this method is referring to the  deleteUser which was called by delete user method in controller class
	 * @param id
	 * @return
	 */
	public boolean deleteUser(int id) {

		try {
			userDao.deleteById(id);
			return true;
		}catch(EmptyResultDataAccessException e) {
			
			e.printStackTrace();
			return false;
			
		}		
	}
	
	@Override
	public List<User> sortByRegistrationDate() {
		
		return userDao.findAll(Sort.by("registrationDate").descending());
		
	}


	@Override
	public List<Object> getUsersPolicy() {
//		List<Object> listOfPolicies = policyDao.getAllUsersPolicy();
//		System.out.println(listOfPolicies.size());
		return null;
		
	}
	


	@Override
	public boolean setDepartment(int userId, int departmentId) {
		System.out.println("Service "+departmentId);
		boolean isUpdate = false;
		Department dept = deptDao.findById(departmentId).get();
		System.out.println(dept.getDeptName());
		int result = deptUpdate.updateDepartment(dept.getId(), userId);
		if(result > 0) {
			isUpdate = true;
		}
		return isUpdate;
	}
	
	
}
