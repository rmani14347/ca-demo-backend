package com.abc.services;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.entities.Address;
import com.abc.entities.User;
import com.abc.repository.AdressRepository;
import com.abc.repository.UserDao;

/**
 * This class is used to perform user services which implements user service 
 * @author 
 *
 */
@Transactional
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userDao;
	
	@Autowired 
	 private AdressRepository adressRepository;

	/**
	 * this method is used to verify the user email whether it is already existed or
	 * not
	 * 
	 * @param email
	 * @return
	 */
	public User getUserByUsername(String username) {

		User userByUsername = userDao.findByUsername(username);
		if (userByUsername != null)
			return userByUsername;
		else
			return null;

	}



	/***
	 * this method is used to update the profile information of the particular user
	 * @param user
	 * @return
	 */
	public User update(User user) {
		
		
		User getUser = getUserByUsername(user.getUsername());
		
		
		if(!getUser.getFirstName().equals(user.getFirstName()))
		{
			getUser.setFirstName(user.getFirstName());
		}
		if(!getUser.getLastName().equals(user.getLastName()))
		{
			getUser.setLastName(user.getLastName());
		}
		
		if(!getUser.getPhone().equals(user.getPhone()))
		{
			getUser.setPhone(user.getPhone());
		}
		
		User updatedUser = userDao.save(getUser);
		
		if(updatedUser!=null)
		return updatedUser;
		else
			return null;
	}
	/**
	 * this method is used to save the user object in the database
	 * 
	 * @param user
	 * @return boolean 
	 */
	

	@Override
	public User save(User user) {
		
		user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
		
		
		
		
		System.out.println(user.getAddress().getDistrict());
	
		User savedUser = userDao.save(user);
		Address adress = adressRepository.save(user.getAddress());
		user.setAddress(adress);
		user.setRegistrationDate(new Timestamp(System.currentTimeMillis()));
		
		if (savedUser != null) {

			return savedUser;
		} else
			return null;
	}

}
