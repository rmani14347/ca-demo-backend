package com.abc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.abc.entities.User;
import com.abc.repository.UserDao;
import com.abc.model.UserInfo;

/**
 * This class is used as an user service which implements UserDetailsService.
 * @author 
 *
 */
public class SecurityService implements UserDetailsService {
	
    @Autowired
    private UserDao userDao;
    
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User userByUsername = userDao.findByUsername(username);
		if(userByUsername!= null) {
		return new UserInfo(userByUsername);
		}
		else
			throw new UsernameNotFoundException("No user found ...");
	}
}
