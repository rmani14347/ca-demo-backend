package com.abc.services;

import java.util.List;

import com.abc.entities.User;

/**
 * This class is used as an interface which has admin services.
 * @author 
 *
 */
public interface AdminService {

	public List<User> displayAllUsers(int page);
	
	public boolean deleteUser(int id);
	
	public List<User> sortByRegistrationDate();
	
	public List<Object> getUsersPolicy();
	
	public boolean setDepartment(int userId , int departmentId);
	
	public long countOfUsersPerPage();
}
