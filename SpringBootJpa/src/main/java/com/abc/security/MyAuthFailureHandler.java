package com.abc.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.abc.payload.ResponseObject;
import com.abc.util.StatusCodes;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class implements AuthenticationEntryPoint.This is used when authentication fails.
 * @author 
 *
 */
public class MyAuthFailureHandler implements AuthenticationEntryPoint {
	
	ResponseObject responseObject = new ResponseObject();
    ObjectMapper objectMapper = new ObjectMapper();
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		
		 responseObject.setResponse(authException.getMessage());
   		 responseObject.setStatusCode(StatusCodes.UN_AUTH.getValue());
		 response.getWriter().write(objectMapper.writeValueAsString(responseObject));
		
	}

}
