package com.abc.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.abc.model.UserInfo;
import com.abc.payload.ResponseObject;
import com.abc.util.StatusCodes;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class implements AuthenticationSuccessHandler.This is used when authentication success.
 * @author 
 *
 */
public class MySuccessHandler implements AuthenticationSuccessHandler {

	ResponseObject responseObject = new ResponseObject();
	ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
		Authentication authentication) throws IOException, ServletException {
	
		responseObject.setResponse(((UserInfo)authentication.getPrincipal()).getUser());
		responseObject.setStatusCode(StatusCodes.OK.getValue());
		//super.onAuthenticationSuccess(request, response, authentication);

		response.getWriter().write(objectMapper.writeValueAsString(responseObject));
		
	}

}
