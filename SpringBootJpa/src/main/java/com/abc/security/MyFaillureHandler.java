package com.abc.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.abc.payload.ResponseObject;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class implements AuthenticationFailureHandler.This is used when authentication fails.
 * @author 
 *
 */
public class MyFaillureHandler implements AuthenticationFailureHandler {
	ResponseObject responseObject = new ResponseObject();
    ObjectMapper objectMapper = new ObjectMapper();
   	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
   		 responseObject.setResponse(exception.getMessage());
   		 responseObject.setStatusCode(2);
		response.getWriter().write(objectMapper.writeValueAsString(responseObject));
		
	}

}
