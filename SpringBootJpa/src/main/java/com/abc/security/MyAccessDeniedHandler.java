package com.abc.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.abc.payload.ResponseObject;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class implements Access Denied Handler.This is used when unauthorized user tries to access
 * @author 
 *
 */
public class MyAccessDeniedHandler implements AccessDeniedHandler{
	
	ResponseObject responseObject = new ResponseObject();
    ObjectMapper objectMapper = new ObjectMapper();
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		responseObject.setResponse(accessDeniedException.getMessage());
		responseObject.setStatusCode(3);
		response.getWriter().write(objectMapper.writeValueAsString(responseObject));
		
	}

}
