package com.abc.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.entities.User;
import com.abc.payload.ResponseObject;
import com.abc.services.AdminService;
import com.abc.services.UserService;
import com.abc.util.StatusCodes;

/**
 * This class refers to admin controller which creates api's for admin services.
 * 
 * @author IMVIZAG
 *
 */
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/admin")
public class AdminController {

	@Autowired
	private AdminService adminService;

	@Autowired
	private UserService userService;

	/**
	 * this method is used for admin to see the all users who are registered
	 * 
	 * @return
	 */

	@GetMapping("/getAllUsers/{page}")
	public ResponseEntity<ResponseObject> displayAllUsers(@PathVariable int page) {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		List<User> userList = adminService.displayAllUsers(page);
		if (userList.size() != 0) {
			responseObject.setResponse(userList);
			responseObject.setStatusCode(StatusCodes.OK.getValue());
		} else {
			responseObject.setResponse("No User Found");
			responseObject.setStatusCode(StatusCodes.ERROR.getValue());

		}

		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
	}

	/*
	 * this method is used for admin to delete the particular user
	 */
	@DeleteMapping("/deleteUser/{username}")
	public ResponseEntity<ResponseObject> deleteUsers(@PathVariable String username) {
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		User userByUsername = userService.getUserByUsername(username);

		if (userByUsername != null) {
			boolean isDeleted = adminService.deleteUser(userByUsername.getId());
			if (isDeleted) {
				responseObject.setResponse("Deleted successfully");
				responseObject.setStatusCode(StatusCodes.OK.getValue());
			} else {

				responseObject.setResponse("user deletion failed");
				responseObject.setStatusCode(StatusCodes.ERROR.getValue());
			}
		} else {
			responseObject.setResponse("User with this Email does not exists");
			responseObject.setStatusCode(StatusCodes.NOT_FOUND.getValue());
		}

		// adding response object and status code of response entity class

		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;

	}

	/*
	 * this method is used for admin to delete the particular user
	 */

	@CrossOrigin()
	@PutMapping("/updateDepartment/{userId}/{departmentId}")
	public ResponseEntity<ResponseObject> assignDepartment(@PathVariable int userId, @PathVariable int departmentId) {
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		boolean result = adminService.setDepartment(userId, departmentId);
		if (result) {
			responseObject.setResponse("Department Set successfully");
			responseObject.setStatusCode(1);
		} else {
			responseObject.setResponse("Department not set");
			responseObject.setStatusCode(StatusCodes.ERROR.getValue());
		}

		// adding response object and status code of response entity class

		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;
	}

	@GetMapping("/getAllUsers/registrationDate")
	public ResponseEntity<ResponseObject> getAllUsersByRegistrationDate() {
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		List<User> userList = adminService.sortByRegistrationDate();
		if (userList.size() != 0) {
			responseObject.setResponse(userList);
			responseObject.setStatusCode(StatusCodes.OK.getValue());
		} else {
			responseObject.setResponse("No User Found");
			responseObject.setStatusCode(StatusCodes.NOT_FOUND.getValue());
		}
		// adding response object and status code of response entity class
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;
	}

	@GetMapping("/getAllUsersPolicies")
	public ResponseEntity<ResponseObject> getAllUsersPolicies() {
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		List<Object> userList = adminService.getUsersPolicy();
		if (userList.size() != 0) {
			responseObject.setResponse(userList);
			responseObject.setStatusCode(StatusCodes.OK.getValue());
		} else {
			responseObject.setResponse("No User Found");
			responseObject.setStatusCode(StatusCodes.NOT_FOUND.getValue());
		}
		// adding response object and status code of response entity class
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;
	}

	@GetMapping("/getCountPerPage")
	public ResponseEntity<ResponseObject> getCountPerPage() {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		long userCount = adminService.countOfUsersPerPage();
		if (userCount != 0) {
			responseObject.setResponse(userCount);
			responseObject.setStatusCode(StatusCodes.OK.getValue());
		} else {
			responseObject.setResponse("No User Found");
			responseObject.setStatusCode(StatusCodes.NOT_FOUND.getValue());
		}
		// adding response object and status code of response entity class
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;
	}
}
