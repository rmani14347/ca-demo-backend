package com.polling.controller;

import java.net.URI;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.polling.entities.User;
import com.polling.model.UserDto;
import com.polling.payload.ApiResponse;
import com.polling.payload.JwtAuthenticationResponse;
import com.polling.payload.LoginRequest;
import com.polling.payload.ResponseObject;
import com.polling.payload.SignUpRequest;
import com.polling.repository.UserRepository;
import com.polling.security.JwtTokenProvider;
import com.polling.service.UserService;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	JwtTokenProvider tokenProvider;

	@PostMapping("/signin")
	public ResponseEntity<ResponseObject> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		ModelMapper modelMapper = new ModelMapper();
		ResponseObject responseObject = new ResponseObject();
		Authentication authentication = null;
		try {
			authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));
			SecurityContextHolder.getContext().setAuthentication(authentication);
			SecurityContextHolder.getContext().setAuthentication(authentication);

			User user = userService.getUserByUsername(loginRequest.getUsernameOrEmail());

			// creating dto class object with required fields
			UserDto userDto = modelMapper.map(user, UserDto.class);

			String jwt = tokenProvider.generateToken(authentication);
			
			responseObject.setResponse(userDto);
			responseObject.setStatusCode(1);
			responseObject.setJwtAuthenticationResponse(new JwtAuthenticationResponse(jwt));
			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		} catch (Exception e) {
			responseObject.setStatusCode(-1);
			responseObject.setResponse("Invalid Username or Password");
			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}

	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ApiResponse(false, "Email Address already in use!"),
					HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(), signUpRequest.getPassword(),
				"ROLE_USER", signUpRequest.getFirstName(), signUpRequest.getLastName(), signUpRequest.getPhone());

		user.setPassword(passwordEncoder.encode(user.getPassword()));

		User result = userRepository.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(result.getUsername()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
	}
}
