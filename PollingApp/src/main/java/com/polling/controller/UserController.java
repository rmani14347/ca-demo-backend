package com.polling.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.polling.entities.User;
import com.polling.model.UserDto;
import com.polling.payload.ResponseObject;
import com.polling.service.UserService;

@RestController
@RequestMapping("api/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	private ResponseObject responseObject = null;
	
	/**
	 * This resource method updates user details
	 * @param user
	 * @return
	 */
	@PutMapping("/updateUserDetails")
	public ResponseEntity<ResponseObject> updateUserDetails(@RequestBody User user)
	{
		int updateStatus = userService.update(user);
		responseObject = new ResponseObject();
		if(updateStatus == -1)
		{
			responseObject.setResponse("Username not found");
			responseObject.setStatusCode(-1);
			return new ResponseEntity<>(responseObject,HttpStatus.OK);
		}
		else if(updateStatus == -2)
		{
			responseObject.setResponse("Updation failed");
			responseObject.setStatusCode(-2);
			return new ResponseEntity<>(responseObject,HttpStatus.OK);
		}
		else
		{
			responseObject.setResponse("Updation Successful");
			responseObject.setStatusCode(1);
			return new ResponseEntity<>(responseObject,HttpStatus.OK);
		}
	}
	
	/**
	 * this method is used to get the details of the particular user by username
	 * @param username
	 * @return user
	 */
	@GetMapping("/getUserByUsername/{username}")
	public ResponseEntity<ResponseObject> getUserByUsername(@PathVariable("username") String username) {
		ModelMapper modelMapper = new ModelMapper();
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		User userByUsername = userService.getUserByUsername(username);

		if (userByUsername != null) {
			UserDto userDto = modelMapper.map(userByUsername, UserDto.class);
			responseObject.setResponse(userDto);
			responseObject.setStatusCode(1);
		} else {

			responseObject.setResponse("User with this User name does not exists");
			responseObject.setStatusCode(-1);

		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
	}

}
