package com.polling.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.polling.entities.User;
import com.polling.model.UserDto;
import com.polling.payload.ResponseObject;
import com.polling.service.AdminService;
import com.polling.service.UserService;

/**
 * This class refers to admin controller which creates api's for admin services.
 * 
 * @author IMVIZAG
 *
 */
@RestController
@RequestMapping("/api/admin")
public class AdminController {

	@Autowired
	private AdminService adminService;

	@Autowired
	private UserService userService;

	/**
	 * this method is used for admin to see the all users who are registered
	 * 
	 * @return
	 */

	@GetMapping("/getusers/")
	public String displayAllUsers() {
		String st = "hello";
		return st;
	}

	/**
	 * This resource method gets list of user
	 * 
	 * @param page count
	 * @return List of user
	 */
	@GetMapping("/getAllUsers/{page}")
	public ResponseEntity<ResponseObject> displayAllUsers(@PathVariable int page) {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		List<User> userList = adminService.displayAllUsers(page);
		if (userList.size() != 0) {
			responseObject.setResponse(userList);
			responseObject.setStatusCode(1);
		} else {
			responseObject.setResponse("No User Found");
			responseObject.setStatusCode(-1);
		}
		// adding response object and status code of response entity class
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;
	}

	/**
	 * This resource method gets all user sort by registration date
	 * 
	 * @return List of User
	 */
	@GetMapping("/getAllUsers/registrationDate")
	public ResponseEntity<ResponseObject> getAllUsersByRegistrationDate() {
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		List<User> userList = adminService.sortByRegistrationDate();
		if (userList.size() != 0) {
			responseObject.setResponse(userList);
			responseObject.setStatusCode(1);
		} else {
			responseObject.setResponse("No User Found");
			responseObject.setStatusCode(-1);
		}
		// adding response object and status code of response entity class
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;
	}

	/**
	 * this method is used to get the details of the particular user by username
	 * 
	 * @param username
	 * @return user
	 */
	@GetMapping("/getUserByUsername/{username}")
	public ResponseEntity<ResponseObject> getUserByUsername(@PathVariable("username") String username) {
		ModelMapper modelMapper = new ModelMapper();
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		User userByUsername = userService.getUserByUsername(username);

		if (userByUsername != null) {
			UserDto userDto = modelMapper.map(userByUsername, UserDto.class);
			responseObject.setResponse(userDto);
			responseObject.setStatusCode(1);
		} else {

			responseObject.setResponse("User with this User name does not exists");
			responseObject.setStatusCode(-1);

		}
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping("/getCountPerPage")
	public ResponseEntity<ResponseObject> getCountPerPage() {

		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject responseObject = new ResponseObject();
		long userCount = adminService.countOfUsersPerPage();
		if (userCount != 0) {
			responseObject.setResponse(userCount);
			responseObject.setStatusCode(1);
		} else {
			responseObject.setResponse("No User Found");
			responseObject.setStatusCode(-1);
		}
		// adding response object and status code of response entity class
		responseEntity = new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		return responseEntity;
	}

}
