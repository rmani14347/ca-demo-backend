package com.polling.service;

import java.util.List;

import com.polling.entities.User;


public interface AdminService {

	public List<User> displayAllUsers(int page);
	
	public List<User> sortByRegistrationDate();
	
	public long countOfUsersPerPage();
	
}
