package com.polling.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.polling.entities.User;
import com.polling.repository.UserRepository;

@Transactional
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	/**
	 * this method is used to verify the user email whether it is already existed or
	 * not
	 * 
	 * @param email
	 * @return
	 */
	public User getUserByUsername(String username) {
		User user = null;
		try {
			 user = userRepository.findByUsername(username).get();
			 return user;
		} catch (Exception e) {
			return null;
		}
	}


	@Override
	/***
	 * this method is used to update the profile information of the particular user
	 * @param profile
	 * @return
	 */
	public int update(User profile) {
		
		
		User getUser = getUserByUsername(profile.getUsername());
		
		if(getUser == null)
		{
			return -1;
		}
		
		if(!getUser.getFirstName().equals(profile.getFirstName()) && profile.getFirstName()!=null)
		{
			getUser.setFirstName(profile.getFirstName());
		}
		if(!getUser.getLastName().equals(profile.getLastName()) && profile.getLastName()!=null)
		{
			getUser.setLastName(profile.getLastName());
		}
		if(!getUser.getUsername().equals(profile.getUsername()) && profile.getUsername()!=null)
		{
			getUser.setUsername(profile.getUsername());
		}
		
		if(!getUser.getPhone().equals(profile.getPhone()) && profile.getPhone()!=null)
		{
			getUser.setPhone(profile.getPhone());
		}
		User updatedUser = userRepository.save(getUser);
		if(updatedUser!=null)
		return 1;
		else
			return -2;
	}
	/**
	 * this method is used to save the user object in the database
	 * 
	 * @param user
	 * @return boolean 
	 */

	@Override
	public List<User> displayAllUsers() {
		List<User> userDetailsList = new ArrayList<User>();
		Iterator<User> i = userRepository.findAll().iterator();
		while(i.hasNext()) {
			userDetailsList.add(i.next());
		}
		
		return userDetailsList;
		
	}


}
