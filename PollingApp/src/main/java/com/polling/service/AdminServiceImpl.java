package com.polling.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.polling.entities.User;
import com.polling.repository.UserDaoNamedQuery;
import com.polling.repository.UserRepository;

/**
 * This class refers to admin service which calls the methods from AdminDAO
 * 
 * @author IMVIZAG
 *
 */
@Service
@Transactional
public class AdminServiceImpl implements AdminService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserDaoNamedQuery userDaoNamedQuery;

	@Override
	/**
	 * this method is referring to the display all user which was called by
	 * controller class
	 * 
	 * @return
	 */
	public List<User> displayAllUsers(int page) {

		List<User> userList = new ArrayList<>();

		Pageable pageable = PageRequest.of(page, 5, Sort.Direction.ASC, "firstName");
		userRepository.findAll(pageable).forEach(userList::add);
		return userList;
	}

	/**
	 * This Method Sort user records by registration date
	 */
	@Override
	public List<User> sortByRegistrationDate() {

		return userDaoNamedQuery.findAllUsers();

	}

	/**
	 * This Method Return no. of user record in database 
	 */
	public long countOfUsersPerPage() {

		List<User> totalUserList = new ArrayList<>();
		userRepository.findAll().forEach(totalUserList::add);
		int noOfUsers = (totalUserList.size()) - 1;
		double UsersPerPage = noOfUsers / 5;
		long countPerPage = Math.round(UsersPerPage);
		return countPerPage + 1;
	}

}
