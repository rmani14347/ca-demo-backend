package com.polling.service;

import java.util.List;

import com.polling.entities.User;


public interface UserService {

	public User getUserByUsername(String username);

	public List<User> displayAllUsers();
	
	public int update(User profile);
}
