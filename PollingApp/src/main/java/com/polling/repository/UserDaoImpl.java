package com.polling.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.polling.entities.User;

/**
 * This Class Implements named Query from JPA
 * 
 * @author IMVIZAG
 *
 */
@Repository
@Transactional
public class UserDaoImpl implements UserDaoNamedQuery {

	@PersistenceContext
	private EntityManager manager;


	@Override
	public List<User> findAllUsers() {
		TypedQuery<User> query = manager.createNamedQuery("User.findAllUsers", User.class);
		List<User> results = query.getResultList();
		return results;
	}

}
