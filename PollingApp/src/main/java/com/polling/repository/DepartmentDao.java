package com.polling.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.polling.entities.Department;

public interface DepartmentDao extends CrudRepository<Department, Integer> {
	
	public Department findByDeptName(String department);
}
