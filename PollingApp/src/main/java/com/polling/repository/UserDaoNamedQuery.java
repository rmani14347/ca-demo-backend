package com.polling.repository;

import java.util.List;

import com.polling.entities.User;

public interface UserDaoNamedQuery {
	List<User> findAllUsers();
}
