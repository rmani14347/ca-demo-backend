package com.polling.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.polling.entities.Policy;


@Repository
public interface PolicyDao extends CrudRepository<Policy, Integer>{

	
//	@Query("SELECT  u.firstName,u.lastName,u.email,u.phone,p.policyName,p.policyDescription FROM Policy p INNER JOIN User u ON p.policyId=u.policyId")
//	public List<Object> getAllUsersPolicy();
	
}